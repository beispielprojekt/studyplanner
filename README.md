<p>
    <a href="https://sopra-ci.cs.tu-dortmund.de/studyplanner/StudyPlanner-shadow.zip"><img alt="Pipeline" src="https://sopra-gitlab.cs.tu-dortmund.de/beispielprojekt/studyplanner/badges/master/pipeline.svg" /></a>
	 <a href="https://sopra-ci.cs.tu-dortmund.de/studyplanner/pmd/main.html"><img alt="Pipeline" src="https://img.shields.io/badge/pmd-0_warnings-success&color=success" /></a>
    <a href="https://sopra-ci.cs.tu-dortmund.de/studyplanner/coverage/"><img alt="Coverage" src="https://sopra-ci.cs.tu-dortmund.de/studyplanner/coverage.svg" /></a>
<!-- 	<a href="https://sopra.cs.tu-dortmund.de/bin/pmd.py?XXY=21A&GROUPNUMBER=4&PROJECT=2"><img alt="Pmd" src="https://sopra-ci.cs.tu-dortmund.de/studyplanner/pmd.svg" /></a>-->
	<a href="https://sopra-ci.cs.tu-dortmund.de/studyplanner/test/"><img alt="JUnit" src="https://sopra-ci.cs.tu-dortmund.de/studyplanner/junit.svg" /></a>
	<a href="https://sopra-ci.cs.tu-dortmund.de/studyplanner/checkstyle/main.html"><img alt="Docs" src="https://sopra-ci.cs.tu-dortmund.de/studyplanner/doc.svg" /></a>
	<a href="https://sopra-ci.cs.tu-dortmund.de/studyplanner/checkstyle/test.html"><img alt="TestDocs" src="https://sopra-ci.cs.tu-dortmund.de/studyplanner/testdoc.svg" /></a>
</p>


# Beispielprojekt StudyPlanner

Dieses Repository enthält den vollständigen Quellcode des Programms StudyPlanner sowie die dazugehörenden Tests.
Ebenfalls sind die Produktbeschreibung und das Astah-Model enthalten.

## Dokumentation

### [Javadoc](https://sopra-ci.cs.tu-dortmund.de/studyplanner/javadoc/)

### [Testdoc](https://sopra-ci.cs.tu-dortmund.de/studyplanner/testjavadoc/)

Hier könnt ihr auch eure Produktbeschreibung verlinken.
<!-- 

## PMD-Bericht

Wenn programmiert wird, vergesst nicht, regelmäßig den PMD-Bericht hier oder direkt in Eclipse zu überprüfen.

[PMD-Bericht](https://sopra.cs.tu-dortmund.de/bin/pmd.py?XXY=21A&GROUPNUMBER=4&PROJECT=2)
-->
